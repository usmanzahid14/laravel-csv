<!DOCTYPE html>
<html lang="en">
<head>
  <title>CSV Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Import Csv File</h2>

  @if(count($errors)>0)
  <div class="alert alert-danger">
    Upload Validation Error<br><br>
    <ul>
      @foreach($errors-all() as error)
      <li>{{$error}}</li>
      @endforeach
    </ul>
  </div>

  @endif


@if($message = Session :: get('success'))
<div class="alert alert-success alert-block">
  <button type="button" class="close" data-dismiss="alert">x</button>
  <strong>{{$message}}</strong>
</div>
@endif
  <form action="{{url('/import_excel/import')}}" method="post" enctype="multipart/form-data">
     
    {{csrf_field()}}
    <div class="form-group">
      <label for="email">Upload File:</label>
      <input type="file" class="form-control" name="select_file">
    </div>
    <div class="form-group">
       <input type="submit" class="btn btn-primary"  class="form-control" name="upload">
    </div>
     
     
   </form>
</div>

<div class="container">
  <h2>Customer Data</h2>
  <p>The .table class adds basic styling (light padding and only horizontal dividers) to a table:</p>            
  <table class="table">
       <tr>
        <th>Customer Name</th>
        <th>Gender</th>
        <th>Address</th>
        <th>City</th>
        <th>Postal Code</th>
        <th>Country</th>
        
      </tr>
      @foreach($data as $row)
        <tr>
          <td>{{$row->CustomerName}}</td>
          <td>{{$row->Gender}}</td>
          <td>{{$row->Address}}</td>
          <td>{{$row->City}}</td>
          <td>{{$row->PostalCode}}</td>
          <td>{{$row->Country}}</td>


        </tr>
 
      endforeach
      
  </table>
</div>

</body>
</html>
